TEMPLATE_REPOSITORY_URL = 'git@bitbucket.org:chimeraprime/rails-template.git'.freeze
TEMPLATE_DIRECTORY = `mktemp -d`.chomp

def source_paths
  super.unshift(TEMPLATE_DIRECTORY)
end

git clone: "#{TEMPLATE_REPOSITORY_URL} #{TEMPLATE_DIRECTORY}"

apply 'template.rb'
